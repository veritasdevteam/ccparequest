﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="CCPA._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="space" Height = "25" HorizontalAlign="center"> </asp:Panel>
                <asp:Panel runat="server" ID="pnlTitle" Height = "50" HorizontalAlign="center">
                    <asp:Label ID="title" runat="server" Text="CCPA Request Search"/>
                </asp:Panel>

            <asp:Panel runat="server" ID="pnlSearch3" HorizontalAlign="Center">
                <asp:Table runat="server" HorizontalAlign="Center">
                    <asp:TableRow Height="20">
                        <asp:TableCell>
                            <asp:Label runat="server" Text="Contract No: "></asp:Label>
                            <asp:TextBox ID="contractNoTextBox" Columns="15" Text="" runat="server"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label runat="server" Text=" OR "></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label runat="server" Text="Last Name: "></asp:Label>
                            <asp:TextBox ID="LNameTextBox" Columns="15" Text="" runat="server"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow HorizontalAlign="Left" Height ="20"> 
                        <asp:TableCell>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label runat="server" Text=" AND "></asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" Text="Zip Code: "></asp:Label>
                            <asp:TextBox ID="zipCodeTextBox" Columns="15" Text="" runat="server"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button runat="server" Text="Search" OnClick="searchButton" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>

            <asp:Panel runat="server" ID="space2" Height = "25" HorizontalAlign="center"> </asp:Panel>

            <asp:Panel runat="server" ID="pnlError" HorizontalAlign="center" visible="false" Height="25">
                <asp:Label runat="server" Text ="Could not locate your customer information. Please try again or call (888) 285-5950 ext. 5701 "></asp:Label>
                <p id="errorMessage" runat="server"></p>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlGreeting" HorizontalAlign="center" Height="25" visible="false">
                <p id="greetingMessage" runat="server"></p>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlResult" HorizontalAlign="center" visible="false">
                <asp:Label runat="server" Text="Would you like Consumer Privacy Protection? "></asp:Label>
                <asp:RadioButton runat="server" ID="yesButton" Text="Yes" GroupName="radioButtons"></asp:RadioButton>
                <asp:RadioButton runat="server" ID="noButton" Text="No" GroupName="radioButtons"></asp:RadioButton>
                <asp:Button runat="server" Text="Submit" OnClick="submitResponse" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSubmitResponse" HorizontalAlign="center" visible="false">
                <p id="submitResponseMessage" runat="server"></p>
            </asp:Panel>

                <asp:HiddenField ID="hfContractNo" runat="server" />
                <asp:HiddenField ID="hfFName" runat="server" />
                <asp:HiddenField ID="hfLName" runat="server" />
                <asp:HiddenField ID="hfZip" runat="server" />


            </ContentTemplate>
        </asp:UpdatePanel>
        
    </form>
    
</body>
</html>
