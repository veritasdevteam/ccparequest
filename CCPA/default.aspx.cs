﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace CCPA
{
    public partial class _default : System.Web.UI.Page
    {
        string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
        string firstname;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void searchButton(object sender, EventArgs e)
        {
            greetingMessage.InnerText = "";
            pnlSubmitResponse.Visible = false;
            pnlError.Visible = false;
            pnlResult.Visible = false;
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            string sSQL = "select * from Contract " +
                         "where (ContractNo = @contractno or LName = @lastname) " +
                         "and zip = @zip ";
            string[] columns = new string[] { "@contractno", "@lastname", "@zip" };
            List<object> varibles = new List<object> { contractNoTextBox.Text.Trim(), LNameTextBox.Text.Trim(), zipCodeTextBox.Text.Trim() };
            SqlDbType[] dbTypes = new SqlDbType[] {SqlDbType.NVarChar, SqlDbType.NVarChar, SqlDbType.NVarChar };
            int count = 0;
            try
            {
                dBO.dboOpen(path);
                DataTable dt = dBO.dboInportInformationSafe(columns,varibles,dbTypes, sSQL);
                count = dt.Rows.Count;
                
                foreach (DataRow dr in dt.Rows)
                {
                    try
                    {
                        hfContractNo.Value = dr["ContractNo"].ToString().Trim();
                        hfFName.Value = dr["FName"].ToString().Trim();
                        hfLName.Value = dr["LName"].ToString().Trim();
                        hfZip.Value = dr["Zip"].ToString().Trim();
                        greetingMessage.InnerText = "Hello " + hfFName.Value + ",";
                        pnlGreeting.Visible = true;
                        pnlResult.Visible = true;
                    } 
                    catch (Exception exc)
                    {
                        errorMessage.InnerText = exc.ToString();
                        pnlGreeting.Visible = false;
                        pnlResult.Visible = false;
                        pnlError.Visible = true;
                    }
                }
                if (dt.Rows.Count == 0)
                {
                    pnlError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage.InnerText = ex.ToString();
                pnlGreeting.Visible = false;
                pnlResult.Visible = false;
                pnlError.Visible = true;
            }
            dBO.dboClose();
        }

        public void submitResponse(object sender, EventArgs e)
        {
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            
            try
            {
                dBO.dboOpen(path);
                if (yesButton.Checked)
                {
                    string sSQL = "update Contract " +
                                  "set CCPA = " + 1 + " " +
                                  "where contractno = '" + hfContractNo.Value + "'";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
                else
                {
                    string sSQL = "update Contract " +
                                  "set CCPA = " + 0 + " " +
                                  "where contractno = '" + hfContractNo.Value + "'";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
                submitResponseMessage.InnerText = "Response Recorded";
                contractNoTextBox.Text = "";
                LNameTextBox.Text = "";
                zipCodeTextBox.Text = "";
                pnlResult.Visible = false;
                pnlSubmitResponse.Visible = true;
                pnlGreeting.Visible = false;
            }
            catch (Exception ex)
            {
                errorMessage.InnerText = ex.ToString();
                pnlGreeting.Visible = false;
                pnlResult.Visible = false;
                pnlError.Visible = true;
            }
            dBO.dboClose();
        }
    }
}